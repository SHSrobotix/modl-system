package modl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class State<Context> implements IMode<Context> {
	protected ModeBase<Context> this_mode;
	protected HashMap<List<Button>, State<Context>> mode_map = new HashMap<List<Button>, State<Context>>();

	public State(ModeBase<Context> mode, HashMap<List<Button>, State<Context>> map) {
		this.mode_map = map;
		this.this_mode = mode;
		
		if(mode == null)
			throw new IllegalArgumentException();
		
		if (map==null)
			mode_map = new HashMap<List<Button>, State<Context>>();
	}
	
	public Iterator<List<Button>> MappedCombos() {
		return mode_map.keySet().iterator();
	}
	
	public void AddCombo(List<Button> combo, State<Context> state) {
		mode_map.put(combo, state);
	}
	
	public void AddStateDefault(State<Context> state) throws Exception {
		List<Button> combo = state.this_mode.DefaultCombo();
		if (combo == null)
			throw new Exception();
		mode_map.put(combo,  state);
	}
	
	public State<Context> IntoNext(List<Button> combo) {
		return mode_map.get(combo);
	}
	
	public void Update(Context context) {
		this_mode.Update(context);
	}

	@Override
	public void EnterMode(Context context) {
		this_mode.EnterMode(context);
	}


	@Override
	public void ExitMode(Context context) {
		this_mode.ExitMode(context);
	}
}
