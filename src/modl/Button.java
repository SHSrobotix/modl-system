package modl;

public class Button {
	
	public int Id() {
		return id;
	}
	
	int id = 0;
	public Button(int id) {
		this.id = id;
	}
	
	@Override
	public int hashCode() {
		return id;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		
		if (!(obj instanceof Button)) {
			return false;
		}
		if(obj == this) {
			return true;
		}
		
		Button rhs = (Button) obj;
		return this.id == rhs.id;
	}
	
	public boolean check (Button other) {
		return id == other.id;
	}
	
	public static Button A() {
		return new Button(0);
	}
	
	public static Button X() {
		return new Button(1);
	}
	
	public static Button Y() {
		return new Button(2);
	}
	
	public static Button B() {
		return new Button(3);
	}
}


