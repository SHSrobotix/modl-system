package modl;

import java.util.List;

public abstract class ModeBase<Context> implements IMode<Context> {
	
	public abstract List<Button> DefaultCombo();
	
	public abstract void EnterMode(Context context);
	public abstract void Update(Context context);
	public abstract void ExitMode(Context context);
}
