package modl;

public interface IMode<Context> {
	public void EnterMode(Context context);
	public void Update(Context context);
	public void ExitMode(Context context);
}
