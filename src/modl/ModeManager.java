package modl;

import java.util.ArrayList;
import java.util.List;

public abstract class ModeManager<Context> {
	
	boolean listening = false;
	List<Button> cur_combo = new ArrayList<Button>();
	
	protected State<Context> current;
	
	public boolean IsListening() {
		return listening;
	}
	
	public abstract void Poll();
	
	protected void Init (State<Context> start) {
		if (start == null) {
			throw new IllegalArgumentException(); //I miss Rust :(
		}
		current = start;
	}
	
	protected void ToggleListening(Context context) {
		if (listening == false) {
			BeginListening();
		} else {
			EndListening(context);
		}
	}
	
	protected void CancelListening() {
		listening = false;
		cur_combo.clear();
	}
	
	protected void BeginListening() {
		if (current == null) {
			throw new NullPointerException();	
			//current should ALWAYS be valid
		}
		listening = true;
		cur_combo.clear();
	}
	
	protected void Update(Context context, Button input) {
		if (listening) {
			if(input!=null) {
				cur_combo.add(input);
			}
		} else {
			current.Update(context);
		}
	}
	
	protected void EndListening(Context context) {
		State<Context> next = current.IntoNext(cur_combo);
		if (next != null) {
			System.out.println("SWITCHING");
			current.ExitMode(context);
			current = next;
			current.EnterMode(context);
		} else {
			System.out.println("Failed to switch");
		}
		CancelListening();
	}
}
